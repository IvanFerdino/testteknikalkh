**Database**

1. Create new DB dengan nama "kh_db"
2. import kh_db.sql ke new database diatas
---


**Frontend http://localhost/index.html**

1. letakkan index.html beserta file javascript lainnya ke localhost (/htdocs) anda
2. pastikan terkoneksi dengan internet, jquery menggunakan CDN

---

**Backend http://localhost:8080**

1. start as Spring project 
2. configurasi data source ada pada /src/main/resources/application.properties
3. spring.datasource.url = jdbc:mysql://localhost:3306/{NAMA DB}?useLegacyDatetimeCode=false&serverTimezone=UTC

---
**Dokumentasi**

1. dokumentasi API selengkapnya dapat dilihat pada https://documenter.getpostman.com/view/8837096/SVn3svPs?version=latest
2. Penggunaan frontend dapat dilihat pada folder screenshot dokumentasi frontend

---

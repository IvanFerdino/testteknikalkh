package com.kontrakhukum.api.controller;


import com.kontrakhukum.api.dao.CustomerDAO;
import com.kontrakhukum.api.exception.ApiRequestException;
import com.kontrakhukum.api.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
public class CustomerController {
    @Autowired
    CustomerDAO customerDAO;

    public boolean checkCustomerId(Integer cust_id){//untuk check apakah cutomer id tersedia/ditemukan
        if(customerDAO.checkId(cust_id)){
            return true;
        }
        return false;
    }

    @GetMapping("/customer") //get all customer
    public List<Customer> getAll(){//return array of customer object
        return customerDAO.getAllCustomer();
    }

    @GetMapping("/customer/all_id") //get all customer id only
    public List getAllId(){ //return array of integer
        return customerDAO.getAllCustomerId();
    }

    @GetMapping("/customer/{id}")//return single customer object by id
    public ResponseEntity getCustomerById(@PathVariable(value = "id") Integer customer_id){
        if(!checkCustomerId(customer_id)){
            throw new ApiRequestException("Customer ID tidak ditemukan!");
        }else{
            Customer customer = customerDAO.getCustomerById(customer_id);
            return ResponseEntity.ok().body(customer);
        }
    }

    @PutMapping("/customer")//insert new customer records
    public ResponseEntity insert(@Valid @RequestBody Map<String, Object> insertObj){
        if(insertObj==null) {//cek insertObj apakah memiliki konten
            throw new ApiRequestException("Something went wrong!");
        }else{
            Customer customer = new Customer();
            customer.setName(insertObj.get("name").toString());
            customer.setPhone(insertObj.get("phone").toString());
            customer.setEmail(insertObj.get("email").toString());
            customerDAO.save(customer);
            return ResponseEntity.ok().build();
        }
    }

    //delete
    @DeleteMapping("/customer/{id}")//delete by id
    public ResponseEntity deleteCustomerById(@PathVariable(value="id") Integer cust_id){
        if(!checkCustomerId(cust_id)){
            throw new ApiRequestException("Customer ID tidak ditemukan!");
        }else{
            Customer customer = customerDAO.getCustomerById(cust_id);
            customerDAO.deleteCustomer(customer);
            return ResponseEntity.ok().build();
        }
    }

    @PostMapping("/customer/{id}")//edit customer records
    public ResponseEntity updateById(@PathVariable(value="id") Integer customer_id, @Valid @RequestBody Map<String, Object> updateObj) {//request body di masukkan kedalam Map, dengan key bentuk String, dan value dalam bentuk Object
        if(!checkCustomerId(customer_id)){
            throw new ApiRequestException("Customer ID tidak ditemukan!");
        }else{
            Customer customer = customerDAO.getCustomerById(customer_id);
            customer.setName(updateObj.get("name").toString());
            customer.setPhone(updateObj.get("phone").toString());
            customer.setEmail(updateObj.get("email").toString());
            customerDAO.save(customer);
            return ResponseEntity.ok().build();
        }
    }
}

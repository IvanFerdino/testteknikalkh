package com.kontrakhukum.api.dao;

import com.kontrakhukum.api.model.Orders;
import com.kontrakhukum.api.model.Ticket;
import com.kontrakhukum.api.repository.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TicketDAO {
    @Autowired
    TicketRepository ticketRepository;

    public boolean checkId(Integer id){
        Ticket tmp = ticketRepository.findById(id).orElse(null);
        if(tmp==null){
            return false;
        }
        return true;
    }

    public List<Ticket> getAllTicket(){// return array of Ticket Objet
        return ticketRepository.findAll();
    }

    public List getAllTicketName(){//return array of string (film name)
        return ticketRepository.getAllTicketName();
    }

    public Ticket getTicketByName(String name){//return single Ticket object, find by film name. exact '=' NOT '%like%'
        return ticketRepository.findByName(name);
    }

    public Ticket getTicketById(Integer id) {
        return ticketRepository.findById(id).orElse(null);
    }

    public Ticket save(Ticket ticket){
        return ticketRepository.save(ticket);
    }

    public Integer getQty(Integer ticket_id){
        return ticketRepository.findTicketQty(ticket_id);
    }

}

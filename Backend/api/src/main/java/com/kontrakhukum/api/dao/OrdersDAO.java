package com.kontrakhukum.api.dao;

import com.kontrakhukum.api.model.Orders;
import com.kontrakhukum.api.repository.OrdersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrdersDAO {
    @Autowired
    OrdersRepository ordersRepository;

    public boolean checkId(Integer id){
        Orders tmp = ordersRepository.findById(id).orElse(null);
        if(tmp==null){
            return false;
        }
        return true;
    }

    public Orders save(Orders orders){
        return ordersRepository.save(orders);
    }

    public Orders getOrdersById(Integer id){
        return ordersRepository.findById(id).orElse(null);
    }

    public List getOrders(){
        return  ordersRepository.getAllOrders();
    }

    public void deleteOrders(Orders orders){
        ordersRepository.delete(orders);
    }
}

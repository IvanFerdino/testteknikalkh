package com.kontrakhukum.api.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.ZoneId;
import java.time.ZonedDateTime;

@ControllerAdvice
public class ApiExceptionHandler {
    @ExceptionHandler(value={ApiRequestException.class})
    public ResponseEntity<Object> handleApiRequestException(ApiRequestException e){//terima exception yg dr depan, yg aslinya, yg belum berformat spt yg diinginkan
        //1. create payload containing exception detail, buat sesuai format yg diinginkan spt di class ApiException nya
        HttpStatus badRequest = HttpStatus.BAD_REQUEST;
        ApiException apiException = new ApiException(e.getMessage(), badRequest, ZonedDateTime.now(ZoneId.of("Z")));
        //2. return responseEntity
        return new ResponseEntity<>(apiException, badRequest); //balikin responsenya
    }
}
package com.kontrakhukum.api.repository;

import com.kontrakhukum.api.model.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TicketRepository extends JpaRepository<Ticket,Integer> {
    @Query(value = "SELECT film FROM ticket", nativeQuery = true)
    List getAllTicketName();

    @Query(value = "SELECT * FROM ticket WHERE film = :name", nativeQuery = true)
    Ticket findByName(String name);

    @Query(value = "SELECT quantity FROM ticket WHERE ticket_id = :ticket_id", nativeQuery = true)
    Integer findTicketQty(Integer ticket_id);
}

package com.kontrakhukum.api.controller;

import com.kontrakhukum.api.dao.CustomerDAO;
import com.kontrakhukum.api.dao.OrdersDAO;
import com.kontrakhukum.api.dao.TicketDAO;
import com.kontrakhukum.api.exception.ApiRequestException;
import com.kontrakhukum.api.model.Orders;
import com.kontrakhukum.api.model.Ticket;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
public class OrdersController {
    @Autowired
    OrdersDAO ordersDAO;

    @Autowired
    TicketDAO ticketDAO;

    @Autowired
    CustomerDAO customerDAO;

    public boolean checkOrdersId(Integer order_id){//untuk check apakah order id tersedia/ditemukan
        if(ordersDAO.checkId(order_id)){
            return true;
        }
        return false;
    }

    public boolean checkCustomerId(Integer cust_id){//untuk check apakah customer id tersedia/ditemukan
        if(customerDAO.checkId(cust_id)){
            return true;
        }
        return false;
    }

    //insert new order
    @PutMapping("/orders")
    public Integer insert(@Valid @RequestBody Map<String, Object> newOrders) {
        if(!checkCustomerId(Integer.parseInt(newOrders.get("customer_id").toString()))){//cek customer id valid atau tidak
            throw new ApiRequestException("Customer ID tidak ditemukan!");
        }else{
            //check movie by id
            if(ticketDAO.getTicketById(Integer.parseInt(newOrders.get("ticket_id").toString()))==null){
                throw new ApiRequestException("Film tidak valid!, mohon coba kembali");
            }else{
                //cek stok
                if(ticketDAO.getQty(Integer.parseInt(newOrders.get("ticket_id").toString()))>=Integer.parseInt(newOrders.get("buy").toString())){//jika stok mencukupi
                    Orders orders = new Orders();
                    orders.setCustomer_id(Integer.parseInt(newOrders.get("customer_id").toString()));
                    orders.setTicket_id(Integer.parseInt(newOrders.get("ticket_id").toString()));
                    orders.setBuy(Integer.parseInt(newOrders.get("buy").toString()));
                    orders = ordersDAO.save(orders);//insert new orders

                    //update ticket stock
                    Ticket ticket = ticketDAO.getTicketById(Integer.parseInt(newOrders.get("ticket_id").toString()));
                    ticket.setQuantity(ticket.getQuantity()-Integer.parseInt(newOrders.get("buy").toString()));
                    ticketDAO.save(ticket);

                    //return order id
                    return orders.getOrder_id();
                }else{//jika stok tidak cukup
                    throw new ApiRequestException("Maaf stok ticket tidak mencukupi, silahkan coba dalam jumlah lain!");
                }
            }
        }

    }

    //get all
    @GetMapping("/orders")
    public List<JSONObject> getAll(){//return array of customer object
        List<Object[]> result = null;//buat List of array of object, karena getOrders() merupakan List yang tersusun dari array of object hasil query
        result = ordersDAO.getOrders();

        //untuk output
        List<JSONObject> output = new ArrayList<JSONObject>();//ini untuk menampung hasil sebelum dikeluarkan/di return

        //di loop hasil dari query nya
        Iterator iter = result.iterator();
        while(iter.hasNext()){
            //masukin ke tmp
            Object[] tmp = (Object[]) iter.next();

            //buat json tmp, untuk menyusun JSON
            JSONObject tmpJson = new JSONObject();

            //ambil isi tmp masukkan ke variable
            Integer order_id = (Integer) tmp[0];
            Integer customer_id = (Integer) tmp[1];
            String name = (String) tmp[2];
            String phone = (String) tmp[3];
            String email = (String) tmp[4];
            Integer ticket_id = (Integer) tmp[5];
            String film = (String) tmp[6];
            Date temp = (Date) tmp[7];
            String date = new SimpleDateFormat("yyyy-MM-dd").format(temp);
            String start_time = (String) tmp[8];
            String finish_time = (String) tmp[9];
            Integer buy = (Integer) tmp[10];

            //susun ulang supaya ada key nya, karna hasil query gada key, hanya value nya saja
            tmpJson.put("order_id",order_id);
            tmpJson.put("customer_id",customer_id);
            tmpJson.put("name",name);
            tmpJson.put("phone",phone);
            tmpJson.put("email",email);
            tmpJson.put("ticket_id",ticket_id);
            tmpJson.put("film",film);
            tmpJson.put("date",date);
            tmpJson.put("start_time",start_time);
            tmpJson.put("finish_time",finish_time);
            tmpJson.put("buy",buy);

            //masukkan JSON baru ke list output
            output.add(tmpJson);
        }
        return output;
    }

    @DeleteMapping("/orders/{orders_id}/{customers_id}")//delete order/refund
    public ResponseEntity deleteOrders(@PathVariable(value="orders_id") Integer order_id, @PathVariable(value="customers_id") Integer customers_id){
        if(!(checkOrdersId(order_id)&&checkCustomerId(customers_id))){//cek order id dan cutomer id hrs sesuai
            throw new ApiRequestException("Order ID / Customer ID tidak ditemukan / tidak sesuai!");
        }else{
            Orders orders = ordersDAO.getOrdersById(order_id);
            Integer ticket_id = orders.getTicket_id();
            Integer buy = orders.getBuy();

            Ticket ticket = ticketDAO.getTicketById(ticket_id);
            ticket.setQuantity(ticket.getQuantity()+buy);//balikin stoknya
            ticketDAO.save(ticket);

            ordersDAO.deleteOrders(orders);

            return ResponseEntity.ok().build();
        }
    }
}

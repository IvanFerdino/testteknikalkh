package com.kontrakhukum.api.repository;

import com.kontrakhukum.api.model.Orders;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrdersRepository extends JpaRepository<Orders,Integer> {
    @Query(value = "SELECT o.order_id AS 'order_id', o.customer_id AS 'customer_id', \n" +
            "e.name AS 'name', e.phone AS 'phone', e.email AS 'email', o.ticket_id AS 'ticket_id',\n" +
            "t.film AS 'film', t.date AS 'date', t.start_time AS 'start_time', t.finish_time AS 'finish_time', o.buy AS 'buy' FROM orders o JOIN customer e ON o.customer_id=e.customer_id JOIN ticket t ON o.ticket_id=t.ticket_id",
            nativeQuery = true)
    List getAllOrders();
}

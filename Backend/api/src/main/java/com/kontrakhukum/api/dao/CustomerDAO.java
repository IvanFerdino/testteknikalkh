package com.kontrakhukum.api.dao;


import com.kontrakhukum.api.model.Customer;
import com.kontrakhukum.api.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerDAO {
    @Autowired
    CustomerRepository customerRepository;

    public boolean checkId(Integer cust_id){
        Customer tmp = customerRepository.findById(cust_id).orElse(null);
        if(tmp==null){
            return false;
        }
        return true;
    }
    public boolean checkPhone(String phone){
        String tmp = customerRepository.getCustomerByPhone(phone);
        if(tmp==""){
            return false;
        }
        return true;
    }
    public boolean checkMail(String email){
        String tmp = customerRepository.getCustomerByEmail(email);
        if(tmp==""){
            return false;
        }
        return true;
    }

    public Customer save(Customer customer){
        return customerRepository.save(customer);
    }

    public List<Customer> getAllCustomer(){
        return customerRepository.findAll();
    }

    public List getAllCustomerId(){
        return customerRepository.getAllCustomerId();
    }

    public Customer getCustomerById(Integer id){
        return customerRepository.findById(id).orElse(null);
    }

    public void deleteCustomer(Customer customer){
        customerRepository.delete(customer);
    }
}


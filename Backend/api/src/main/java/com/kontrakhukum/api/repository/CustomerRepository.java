package com.kontrakhukum.api.repository;

import com.kontrakhukum.api.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer> {

    @Query(value = "SELECT customer_id FROM customer", nativeQuery = true)
    List getAllCustomerId();

    @Query(value = "SELECT phone FROM customer where phone = :phone", nativeQuery = true)
    String getCustomerByPhone(String phone);

    @Query(value = "SELECT email FROM customer where email = :email", nativeQuery = true)
    String getCustomerByEmail(String email);
}

package com.kontrakhukum.api.controller;

import com.kontrakhukum.api.dao.TicketDAO;
import com.kontrakhukum.api.exception.ApiRequestException;
import com.kontrakhukum.api.model.Ticket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@RestController
public class TicketController {
    @Autowired
    TicketDAO ticketDAO;

    public boolean checkTicketId(Integer id){//untuk check apakah ticket id tersedia/ditemukan
        if(ticketDAO.checkId(id)){
            return true;
        }
        return false;
    }

    @GetMapping("/ticket")//get all ticket records
    public List<Ticket> getAll(){
        return ticketDAO.getAllTicket();
    }

    @GetMapping("/ticket/all_name")// get all ticket/ film name only
    public List<Ticket> getAllTicketName(){
        return ticketDAO.getAllTicketName();
    }

    @GetMapping("/ticket/{name}")//get ticket object by ticket/film name
    public ResponseEntity getTicketByName(@PathVariable(value = "name") String film_name){
        Ticket ticket = ticketDAO.getTicketByName(film_name);
        if(ticket==null){
            throw new ApiRequestException("Film name tidak ditemukan!");
        }else{
            return ResponseEntity.ok().body(ticket);
        }
    }

    //insert new ticket
    @PutMapping("/ticket")
    public ResponseEntity insert(@Valid @RequestBody Map<String, Object> insertObj){
        if(insertObj==null) {//cek insertObj apakah memiliki konten
            throw new ApiRequestException("Something went wrong");
        }else{
            if(ticketDAO.getTicketByName(insertObj.get("film").toString())!=null){
                throw new ApiRequestException("Film name sudah ada, gunakan nama lain!");
            }else{
                try {
                    Date date=new SimpleDateFormat("dd/MM/yyyy").parse(insertObj.get("date").toString());//parse string ke java.util.date
                    java.sql.Date sqlDate = new java.sql.Date(date.getTime());//parse ke java.sql.date dari java.util.date

                    Ticket ticket = new Ticket();
                    ticket.setFilm(insertObj.get("film").toString());
                    ticket.setDate(sqlDate);
                    ticket.setStart_time(insertObj.get("start_time").toString());
                    ticket.setFinish_time(insertObj.get("finish_time").toString());
                    ticket.setQuantity(Integer.parseInt(insertObj.get("quantity").toString()));
                    ticketDAO.save(ticket);
                    return ResponseEntity.ok().build();
                } catch (ParseException e) {
                    e.printStackTrace();
                    throw new ApiRequestException("Something went wrong");
                }
            }
        }
    }

    //edit ticket record
    @PostMapping("/ticket/{id}")
    public ResponseEntity updateById(@PathVariable(value="id") Integer ticket_id, @Valid @RequestBody Map<String, Object> updateObj) {//request body di masukkan kedalam Map, dengan key bentuk String, dan value dalam bentuk Object
        if(!checkTicketId(ticket_id)){
            throw new ApiRequestException("Ticket ID tidak ditemukan!");
        }else{
            Ticket ticket = ticketDAO.getTicketById(ticket_id);
            ticket.setQuantity(ticket.getQuantity()+Integer.parseInt(updateObj.get("quantity").toString()));
            ticketDAO.save(ticket);
            return ResponseEntity.ok().build();
        }
    }
}
